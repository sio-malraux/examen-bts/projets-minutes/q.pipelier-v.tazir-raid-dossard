#include "../header/logger.h"

Logger::Logger(bool ptr_only, std::string _file):
        file(_file)
{
    if(!ptr_only){
        log4cxx::FileAppender * file_appender = new log4cxx::FileAppender( log4cxx::LayoutPtr(new log4cxx::PatternLayout("%d{%Y-%m-%d %H:%M:%S} %p - %m%n")), "raid-dossard.log", true);

        log4cxx::BasicConfigurator::configure(log4cxx::AppenderPtr(file_appender));
        log4cxx::Logger::getRootLogger()->setLevel(log4cxx::Level::getDebug());
    }

    logger = log4cxx::Logger::getLogger("logger");
}

Logger::~Logger(){}

void Logger::log_debug(std::string str){
    LOG4CXX_DEBUG(logger, file << " : " << str);
}

void Logger::log_info(std::string str){
    LOG4CXX_INFO(logger, file << " : " << str);
}

void Logger::log_warn(std::string str){
    LOG4CXX_WARN(logger, file << " : " << str);
}

void Logger::log_error(std::string str){
    LOG4CXX_ERROR(logger, file << " : " << str);
}

void Logger::log_fatal(std::string str){
    LOG4CXX_FATAL(logger, file << " : " << str);
}

void Logger::log_trace(std::string str){
    LOG4CXX_TRACE(logger, file << " : " << str);
}
