#include "../header/file_configuration.h"

FileConfiguration::FileConfiguration():
    config_db_host("host"),
    config_db_name("name"),
    config_db_user("user"),
    config_db_password("password"),
    config_db_schema("schema"),
    config_db_port("5432"),
    config_loaded("false")
{}

void FileConfiguration::load(std::string path){
    Json::Value config;
    std::ifstream file;
    file.open(path);
    std::string errors;
    Json::CharReaderBuilder builder_json;
    builder_json["collectComments"] = false;

    if(file.is_open()){
        config_loaded = true;

        if(Json::parseFromStream(builder_json, file, &config, &errors)){
            config_db_host = config["database"].get("hostname", config_db_host).asString();
            config_db_user = config["database"].get("user", config_db_user).asString();
            config_db_name = config["database"].get("name", config_db_name).asString();
            config_db_schema = config["database"].get("schema", config_db_schema).asString();
            config_db_password = config["database"].get("password", config_db_password).asString();
            config_db_port = config["database"].get("port", config_db_password).asString();
        }

        file.close();
    }
}

bool FileConfiguration::getConfigLoaded()
{
    return config_loaded;
}

std::string FileConfiguration::getDbHost()
{
    return config_db_host;
}

std::string FileConfiguration::getDbName()
{
    return config_db_name;
}

std::string FileConfiguration::getDbUser()
{
    return config_db_user;
}

std::string FileConfiguration::getDbPassword()
{
    return config_db_password;
}

std::string FileConfiguration::getDbSchema()
{
    return config_db_schema;
}

std::string FileConfiguration::getDbPort()
{
    return config_db_port;
}
