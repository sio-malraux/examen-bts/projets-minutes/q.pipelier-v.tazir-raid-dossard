#include <stdlib.h>
#include "../header/team_integration.h"

TeamIntegration::TeamIntegration(int number, std::string raid_code, std::string licence, std::string schema, PGconn *connection)
{
  std::string rq = "SELECT temps_individuel, num_dossard FROM \"" + schema + "\".\"integrer_equipe\" WHERE numero_equipe = $1 AND code_raid = $2";
  const char *values[] {std::to_string(number).c_str(), raid_code.c_str(), licence.c_str()};
  const Oid types[] = {25};
  PGresult *result_rq = PQexecParams(
    connection,
    rq.c_str(),
    1,
    types,
    values,
    nullptr,
    nullptr,
    0);

  this->individual_time = atoi(PQgetvalue(result_rq, 0, 0));
  this->shirt_number = PQgetvalue(result_rq, 0, 1);
  this->the_runner = new Runner(licence, schema, connection);
  this->the_team = new Team(number, raid_code, schema, connection);
}

TeamIntegration::TeamIntegration(Runner *the_runner, Team *the_team, std::string schema, PGconn *connection):
  the_runner(the_runner),
  the_team(the_team)
{
  std::string rq = "SELECT temps_individuel, num_dossard FROM \"" + schema + "\".\"integrer_equipe\" WHERE numero_equipe = $1 AND code_raid = $2";
  const char *values[] {std::to_string(the_team->getNumber()).c_str(), the_team->getTheRaid()->getCode().c_str(), the_runner->getLicence().c_str()};
  const Oid types[] = {25};
  PGresult *result_rq = PQexecParams(
    connection,
    rq.c_str(),
    1,
    types,
    values,
    nullptr,
    nullptr,
    0);

  this->individual_time = atoi(PQgetvalue(result_rq, 0, 0));
  this->shirt_number = PQgetvalue(result_rq, 0, 1);
}

TeamIntegration::~TeamIntegration(){}

Runner * TeamIntegration::getTheRunner()
{
  return the_runner;
}

Team * TeamIntegration::getTheTeam()
{
  return the_team;
}

int TeamIntegration::getIndividualTime()
{
  return individual_time;
}

std::string TeamIntegration::getShirtNumber()
{
  return shirt_number;
}

void TeamIntegration::setTheRunner(Runner * _the_runner)
{
  the_runner = _the_runner;
}

void TeamIntegration::setTheTeam(Team * _the_team)
{
  the_team = _the_team;
}

void TeamIntegration::setIndividualTime(int _individual_time)
{
  individual_time = _individual_time;
}

void TeamIntegration::setShirtNumber(std::string _shirt_number)
{
  shirt_number = _shirt_number;
}

bool TeamIntegration::hasShirtNumber()
{
  return (shirt_number == "");
}
