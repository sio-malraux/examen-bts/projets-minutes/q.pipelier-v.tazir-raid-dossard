#include <string.h>
#include "../header/runner.h"

Runner::Runner(std::string licence, std::string schema, PGconn *connection)
{
  std::string rq = "SELECT licence, nom, prenom, sexe, nationalite, mail, certif_med_aptitude, date_naissance FROM \"" + schema + "\".\"coureur\" WHERE licence = $1;";
  const char *values[] {licence.c_str()};
  const Oid types[] = {25};
  PGresult *result_rq = PQexecParams(
    connection,
    rq.c_str(),
    1,
    types,
    values,
    nullptr,
    nullptr,
    0);

  this->licence = PQgetvalue(result_rq, 0, 0);
  this->name = PQgetvalue(result_rq, 0 ,1);
  this->surname = PQgetvalue(result_rq, 0, 2);
  this->gender = PQgetvalue(result_rq, 0, 3)[0];
  this->nationality = PQgetvalue(result_rq, 0, 4);
  this->mail = PQgetvalue(result_rq, 0, 5);
  this->certif_med_aptitude = (strcmp(PQgetvalue(result_rq, 0, 6), "true") == 0); 
  this->birthdate = PQgetvalue(result_rq, 0, 7);
}

Runner::~Runner(){}

std::string Runner::getLicence()
{
  return licence;
}

std::string Runner::getName()
{
  return name;
}

std::string Runner::getSurname()
{
  return surname;
}

char Runner::getGender()
{
  return gender;
}

std::string Runner::getNationality()
{
  return nationality;
}

std::string Runner::getMail()
{
  return mail;
}

bool Runner::getCertifMedAptitude()
{
  return certif_med_aptitude;
}

std::string Runner::getBirthdate()
{
  return birthdate;
}

void Runner::setLicence(std::string _licence)
{
  licence = _licence;
}

void Runner::setName(std::string _name)
{
  name = _name;
}

void Runner::setSurname(std::string _surname)
{
  surname = _surname;
}

void Runner::setGender(char _gender)
{
  gender = _gender;
}

void Runner::setNationality(std::string _nationality)
{
  nationality = _nationality;
}

void Runner::setMail(std::string _mail)
{
  mail = _mail;
}

void Runner::setCertifMedAptitude(bool _certif_med_aptitude)
{
  certif_med_aptitude = _certif_med_aptitude;
}

void Runner::setBirthdate(std::string _birthdate)
{
  birthdate = _birthdate;
}
