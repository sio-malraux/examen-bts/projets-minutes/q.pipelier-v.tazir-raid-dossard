//
// Created by quentin on 08/05/2020.
//

#include "../header/generator.h"
#include "../header/raid.h"

Generator::Generator(std::string code_raid, std::string schema, PGconn *connection):
  connection(connection),
  schema(schema),
  code_raid(code_raid)
{
    Raid *raid = new Raid(code_raid, schema, connection);

    if(raid->isExist()){
        this->exist = true;
        std::string req = "SELECT num_dossard FROM \""+ schema +"\".\"integrer_equipe\" WHERE num_dossard ~ $1;";
        const char *val[] {code_raid.c_str()};
        const Oid types[] = {25};
        PGresult *result_req = PQexecParams(
                connection,
                req.c_str(),
                1,
                types,
                val,
                NULL,
                NULL,
                0);

        if(PQntuples(result_req) > 0){
            this->generated = true;
        }
    }
}

Generator::~Generator(){}

std::string Generator::generateDossard(){
    if(this->exist == false){
        this->result_message = "Raid number parameter doesn't exist.";
    }else if(this->generated == true){
        this->result_message = "Shirt numbers have already been generated for this raid.";
    }else{
      std::vector<std::string> all_num_dossard = generateDossardString();
      for(std::string str : all_num_dossard){
        std::cout << str << std::endl;
      }
      this->result_message = "The shirt number have successfully been generated.";
    }

    return this->result_message;
}

std::vector<std::string> Generator::generateDossardString(){
  std::string rq = "SELECT numero_equipe, \"equipe\".code_raid, \"coureur\".nom, \"coureur\".prenom, num_dossard, \"coureur\".licence FROM \"" + schema + "\".\"integrer_equipe\" INNER JOIN \"" + schema + "\".\"equipe\" ON \"integrer_equipe\".numero_equipe = \"equipe\".numero INNER JOIN \"" + schema + "\".\"coureur\" ON \"coureur\".licence = \"integrer_equipe\".licence WHERE \"equipe\".code_raid = $1 ORDER BY numero_equipe, \"equipe\".code_raid;";

  std::vector<std::string> all_dossard_string;
  
  const char *val[] {code_raid.c_str()};
  const Oid types[] = {25};
  PGresult *result_rq = PQexecParams(
           this->connection,
           rq.c_str(),
           1,
           types,
           val,
           nullptr,
           nullptr,
           0);

  if(PQntuples(result_rq) > 0){
    int team_number = atoi(PQgetvalue(result_rq, 0, 0));

    for (int i = 0, j = 1; i < PQntuples(result_rq); i++, j++){
      if(atoi(PQgetvalue(result_rq, i, 0)) != team_number){
        j = 1;
        team_number = atoi(PQgetvalue(result_rq, i, 0));
      }
      std::string runner_name = PQgetvalue(result_rq, i, 2);
      std::string runner_surname = PQgetvalue(result_rq, i, 3);

      std::string dossard_string = std::to_string(team_number) + "-" + code_raid + "-" + runner_surname[0] + runner_name[0] + "-" + std::to_string(j);
      all_dossard_string.push_back(dossard_string);

      std::string rq_update = "UPDATE \"" + schema + "\".\"integrer_equipe\" SET num_dossard = $1 WHERE numero_equipe = $2 AND code_raid = $3 AND licence = $4";
      const char *values[] {dossard_string.c_str(), std::to_string(team_number).c_str(), code_raid.c_str(), PQgetvalue(result_rq, i, 5)};
      const Oid typs[] = {25, 23, 25, 25};
      PGresult *result_update = PQexecParams(
        this->connection,
        rq_update.c_str(),
        4,
        typs,
        values,
        nullptr,
        nullptr,
        0);

      if (PQresultStatus(result_update) != PGRES_COMMAND_OK) {
        // TODO : afficher le message d'erreur dans le log avec la commande : PQresultErrorMessage(result_update);
      }
    }
  }
  return all_dossard_string;
}
