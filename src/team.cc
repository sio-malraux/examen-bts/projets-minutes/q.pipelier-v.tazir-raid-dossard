#include <stdlib.h>
#include "../header/team.h"

Team::Team(int number, std::string code_raid, std::string schema, PGconn *connection)
{
  std::string rq = "SELECT numero, code_raid, nom, date_inscription, temps_global, classement, penalites_bonif FROM \"" + schema + "\".\"equipe\" WHERE numero = $1 AND code_raid = $2";
  const char *values[] {std::to_string(number).c_str(), code_raid.c_str()};
  const Oid types[] = {23, 1043};
  PGresult *result_rq = PQexecParams(
    connection,
    rq.c_str(),
    2,
    types,
    values,
    nullptr,
    nullptr,
    0);

  this->number = atoi(PQgetvalue(result_rq, 0, 0));
  this->the_raid = new Raid(std::string(PQgetvalue(result_rq, 0, 1)), schema, connection);
  this->name = PQgetvalue(result_rq, 0, 2);
  this->record_date = PQgetvalue(result_rq, 0, 3);
  this->global_time = atoi(PQgetvalue(result_rq, 0, 4));
  this->ranking = atoi(PQgetvalue(result_rq, 0, 5));
  this->penalties = atoi(PQgetvalue(result_rq, 0, 6));
}

Team::~Team(){};

int Team::getNumber()
{
  return number;
}

Raid *Team::getTheRaid()
{
  return the_raid;
}

std::string Team::getName()
{
  return name;
}

std::string Team::getRecordDate()
{
  return record_date;
}

int Team::getGlobalTime()
{
  return global_time;
}

int Team::getRanking()
{
  return ranking;
}

int Team::getPenalties()
{
  return penalties;
}

void Team::setNumber(int _number)
{
  number = _number;
}

void Team::setTheRaid(Raid *_the_raid)
{
  the_raid = _the_raid;
}

void Team::setName(std::string _name)
{
  name = _name;
}

void Team::setRecordDate(std::string _record_date)
{
  record_date = _record_date;
}

void Team::setGlobalTime(int _global_time)
{
  global_time = _global_time;
}

void Team::setRanking(int _ranking)
{
  ranking = _ranking;
}

void Team::setPenalties(int _penalties)
{
  penalties = _penalties;
}
