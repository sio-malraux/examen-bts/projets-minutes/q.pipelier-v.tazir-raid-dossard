#include <iostream>
#include <postgresql/libpq-fe.h>
#include <gflags/gflags.h>
#include "../header/file_configuration.h"
#include "../header/cli_manager.h"
#include "../header/logger.h"
#include "../header/generator.h"

int main(int argc, char *argv[]){
    Logger logger(false, "main.cc");
    logger.log_debug("BEGIN");

    //cli manager
    configureCommandLine();
    gflags::ParseCommandLineFlags(&argc, &argv, true);

    //set defaults values by gflags
    std::string db_host = FLAGS_db_host;
    std::string db_user = FLAGS_db_user;
    std::string db_password = FLAGS_db_password;
    std::string db_schema = FLAGS_db_schema;
    std::string db_name = FLAGS_db_name;
    std::string db_port = FLAGS_db_port;
    std::string config_path = FLAGS_config_file;
    std::string raid_code = FLAGS_raid_code;

    //get config
    FileConfiguration *config_file = new FileConfiguration();
    config_file->load(config_path);

    if(config_file->getConfigLoaded()){
        db_host = config_file->getDbHost();
        db_user = config_file->getDbUser();
        db_password = config_file->getDbPassword();
        db_schema = config_file->getDbSchema();
        db_name = config_file->getDbName();
        db_port = config_file->getDbPort();

        logger.log_info("Config was loaded");
    }

    PGconn *data_connect = PQsetdbLogin(db_host.c_str(), db_port.c_str(), nullptr, nullptr, db_name.c_str(), db_user.c_str(), db_password.c_str());

    if(PQstatus(data_connect) != CONNECTION_OK){
        logger.log_fatal("Database connection failed: " + db_host + ":" + db_port);
    }else{
        logger.log_info("Database connection success");

        Generator *generator = new Generator(raid_code, db_schema, data_connect);
        std::cout << generator->generateDossard();
    }

    //shutdown gflags line
    gflags::ShutDownCommandLineFlags();
    logger.log_debug("END");

    return 0;
}