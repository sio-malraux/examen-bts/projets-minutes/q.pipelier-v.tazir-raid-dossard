#include "../header/raid.h"

Raid::Raid(std::string code, std::string schema, PGconn *connection){
    std::string req = "SELECT nom, date_debut, ville, region, nb_maxi_par_equipe, montant_inscription, nb_femmes, duree_maxi, age_minimum FROM \""+ schema +"\".\"raid\" WHERE code = $1;";
    const char *val[] {code.c_str()};
    const Oid types[] = {25};
    PGresult *result_req = PQexecParams(
            connection,
            req.c_str(),
            1,
            types,
            val,
            NULL,
            NULL,
            0);

    if(PQntuples(result_req) > 0){
      this->name = PQgetvalue(result_req, 0, 0);
      this->started_date = PQgetvalue(result_req, 0, 1);
      this->city = PQgetvalue(result_req, 0, 2);
      this->region = PQgetvalue(result_req, 0, 3);
      this->max_runners_by_team = atoi(PQgetvalue(result_req, 0, 4));
      this->price = std::stod(PQgetvalue(result_req, 0, 5));
      this->number_female = atoi(PQgetvalue(result_req, 0, 6));
      this->max_duration = atoi(PQgetvalue(result_req, 0, 7));
      this->old_minimum = atoi(PQgetvalue(result_req, 0, 8));
      this->exist = true;
    }
}

Raid::~Raid(){}

std::string Raid::getCode(){
  return code;
}

std::string Raid::getName(){
    return name;
}

std::string Raid::getStartedDate(){
    return started_date;
}

std::string Raid::getCity(){
    return city;
}

std::string Raid::getRegion(){
    return region;
}

int Raid::getMaxRunners(){
    return max_runners_by_team;
}

double Raid::getPrice(){
    return price;
}

int Raid::getNumberFemale(){
    return number_female;
}

int Raid::getMaxDuration(){
    return max_duration;
}

int Raid::getOldMinimum(){
    return old_minimum;
}

bool Raid::isExist(){
    return exist;
}
