#include <iostream>
#include "../header/cli_manager.h"

DEFINE_string(db_host, "localhost", "Hostname database");
DEFINE_string(db_name, "database name", "Name database");
DEFINE_string(db_schema, "schema", "Schema database");
DEFINE_string(db_user, "user", "User database");
DEFINE_string(db_password, "password", "Password database");
DEFINE_string(db_port, "port", "Port database");
DEFINE_string(config_file, "config/package.json", "Path config");
DEFINE_string(raid_code, "0000", "Raid code");

void configureCommandLine(){
    gflags::SetVersionString("1.0.0");
}