#include "../header/activite.h"

Activity::Activity(std::string code, std::string libel, std::vector<Raid> raids):
code(code),
libel(libel),
raids(raids)
{}

Activity::~Activity() {}

std::string Activity::getCode()
{
    return code;
}

std::string Activity::getLibel()
{
    return libel;
}

std::vector<Raid> Activity::getRaids()
{
    return raids;
}

void Activity::setCode(std::string code)
{
    this->code = code;
}

void Activity::setLibel(std::string libel)
{
    this->libel = libel;
}

void Activity::setRaids(std::vector<Raid> raids)
{
    this->raids = raids;
}
