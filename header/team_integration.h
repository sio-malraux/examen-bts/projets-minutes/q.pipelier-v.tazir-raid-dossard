#ifndef TEAM_INTEGRATION_H
#define TEAM_INTEGRATION_H
#include <string>
#include <postgresql/libpq-fe.h>
#include "team.h"
#include "runner.h"

/**
 * \class TeamIntegration
 *
 * \brief Link the Team class and the Runner class + stores the time and the shirt_number of a Runner in his team
 */
class TeamIntegration{
private:
  Runner *the_runner;
  Team *the_team;
  int individual_time;
  std::string shirt_number;

public:

  /**
   * \brief Constructor which initialize the attributes with the values of the parameters
   *
   * \param std::string number : Number of the team
   *
   * \param std::string raid_code : Code of the raid
   *
   * \param std::string licence : icence of the runner
   *
   * \param std::string schema : Schema of the database in which to retrieve the data
   *
   * \param Pgconn* connexion : handler for the database connexion
   */
  TeamIntegration(int, std::string, std::string, std::string, PGconn *);
  
  /**
   * \brief Other constructor using objects instead of attributes
   *
   * \param Runner* the_runner : Runner object
   *
   * \param Team* the_team : Team object
   *
   * \param std::string schema : Schema of the database in which to retrieve the data
   *
   * \param Pgconn * connexion : handler for the database connexion
   */
  TeamIntegration(Runner *, Team *, std::string, PGconn *);
  
  /**
	* \brief Destructor
	*/
  ~TeamIntegration();

  /**
   * \brief Get the runner
   *
   * \return the_runner : the runner of the raid Runner * object
   */
  Runner *getTheRunner();
  
  /**
   * \brief Get the team
   *
   * \return the_team : the team as a Team * object
   */
  Team *getTheTeam();
  
  /**
   * \brief Get the individual time
   *
   * \return individual_time : the individual time of the runner in his team as a std::string
   */
  int getIndividualTime();
  
  /**
   * \brief Get the shirt number
   *
   * \return shirt_number : the shirt number of the runner in his team as a std::string
   */
  std::string getShirtNumber();
  
   /**
   * \brief Mutate the the_runner attribute with the value of the parameter
   *
   * \param Runner* the_runner - the new runner
   */
  void setTheRunner(Runner *);
  
   /**
   * \brief Mutate the the_team attribute with the value of the parameter
   *
   * \param Team* the_team - the new team
   */
  void setTheTeam(Team *);
  
   /**
   * \brief Mutate the individual time attribute with the value of the parameter
   *
   * \param std::string individual_time - the new individual time
   */
  void setIndividualTime(int);
  
   /**
   * \brief Mutate the shirt_number attribute with the value of the parameter
   *
   * \param std::string shirt_number - the new shirt number
   */
  void setShirtNumber(std::string);

  bool hasShirtNumber();
};
#endif //TEAM_INTEGRATION_H
