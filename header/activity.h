#include <vector>
#include <string>
#include "raid.h"

#ifndef ACTIVITY_H
#define ACTIVITY_H

/**
 * \class Activity
 * \brief Unused class - Represents an Activity
 */
class Activity {

private:
    std::string code;
    std::string libel;
    std::vector<Raid> raids;

public:
  /**
   * \brief Get the code of the Activity
   *
   * \return code : the id of the activity as a std::string
   */
    std::string getCode();
	
  /**
   * \brief Get the libel of the Activity
   *
   * \return libel : the libel of the activity as a std::string
   */
    std::string getLibel();
	
  /**
   * \brief Get all of the Raid objects related to the Activity
   *
   * \return raids : a list of every raid concerned by the activity as a std::vector
   */
    std::vector<Raid> getRaids();

  /**
   * \brief Mutate the code attribute with the value of the parameter
   *
   * \param std::string code - the new code of the Activity
   */
    void setCode(std::string);	
	
  /**
   * \brief Mutate the libel attribute with the value of the parameter
   *
   * \param std::string libel - the new libel of the Activity
   */
    void setLibel(std::string);
	
  /**
   * \brief Mutate the raids attribute with the value of the parameter
   *
   * \param std::vector<Raid> raids - the new list of Raid of the Activity
   */
    void setRaids(std::vector<Raid>);
};

#endif	//ACTIVITY_H
