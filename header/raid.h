//
// Created by quentin on 05/05/2020.
//

#ifndef Q_PIPELIER_V_TAZIR_RAID_DOSSARD_RAID_H
#define Q_PIPELIER_V_TAZIR_RAID_DOSSARD_RAID_H

#include <iostream>
#include <string>
#include <postgresql/libpq-fe.h>
#include <stdlib.h>

class Raid {
private:
    std::string code;
    std::string name;
    std::string started_date;
    std::string city;
    std::string region;
    int max_runners_by_team;
    double price;
    int number_female;
    int max_duration;
    int old_minimum;
    bool exist = false;

public:
  /**
   * \brief Unique Constructor which initialize the attributes with the values of the parameters
   *
   * \param std::string code : Code of the raid
   *
   * \param std::string schema : Schema of the database in witch to retrieve the data
   *
   * \param Pgconn * connexion : handler for the database connexion
   */
    Raid(std::string, std::string, PGconn *);

   /**
	* \brief Destructor
	*/
    ~Raid();

  /**
   * \brief Get the code of the Raid
   *
   * \return code : the code of the raid as a std::string
   */
    std::string getCode();
	
  /**
   * \brief Get the name of the Raid
   *
   * \return name : the name of the raid as a std::string
   */
    std::string getName();
	
  /**
   * \brief Get the begin date of the Raid
   *
   * \return started_date : the begin date of the raid as a std::string
   */
    std::string getStartedDate();
	
  /**
   * \brief Get the city of the Raid
   *
   * \return city : the city where the raid will be located as a std::string
   */
    std::string getCity();
	
  /**
   * \brief Get the region of the Raid
   *
   * \return region : the region where the raid wil be located as a std::string
   */
    std::string getRegion();
	
  /**
   * \brief Get the maximum number of runner per team in the Raid
   *
   * \return max_runners_by_team : the maximum number of runner per team in the raid as an integer
   */
    int getMaxRunners();
	
  /**
   * \brief Get the price of participation of the Raid
   *
   * \return price : the price of participation of the raid as a double
   */
    double getPrice();
	
  /**
   * \brief Get the number of female of the Raid
   *
   * \return number_female : the total number of female allowed to participate in the raid as an integer
   */
    int getNumberFemale();
	
  /**
   * \brief Get the maximum duration of the Raid
   *
   * \return max_duration : the maximum duration of the raid in days as an integer
   */
    int getMaxDuration();
	
  /**
   * \brief Get the minimum age of the Raid
   *
   * \return old_minimum : the minimal age required to participate in the raid as an integer
   */
    int getOldMinimum();
	
  /**
   * \brief Allow to know if the raid exist in the database
   *
   * \return exist : true if the raid exist in the database, false otherwise
   */
    bool isExist();
};
#endif //Q_PIPELIER_V_TAZIR_RAID_DOSSARD_RAID_H
