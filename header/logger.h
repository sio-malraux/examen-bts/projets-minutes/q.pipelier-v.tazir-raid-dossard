//
// Created by quentin on 06/05/2020.
//

#ifndef Q_PIPELIER_V_TAZIR_RAID_DOSSARD_LOGGER_H
#define Q_PIPELIER_V_TAZIR_RAID_DOSSARD_LOGGER_H

#include <string.h>
#include <log4cxx/logger.h>
#include <log4cxx/stream.h>
#include <log4cxx/basicconfigurator.h>
#include <log4cxx/fileappender.h>
#include <log4cxx/patternlayout.h>
#include <log4cxx/helpers/exception.h>

/**
 * \class Logger
 * \brief used to logging
 *
 * Logging is made on a file named 'raid-dossard.log' located at the root of the project
 */
class Logger{

private :
    log4cxx::LoggerPtr logger;
    std::string file;

public:
    /**
     * \brief Unique constructor of log objects
     *
     * \param bool ptr_only : type false if you want to configure a new logger (Warning must use once per project)
     * \param std::string _file : Name of the file where the object is created
     */
    Logger(bool, std::string);

    /**
     * \brief Destructor
     */
    ~Logger();

    /**
     * \brief Write the string passed in parameter as an info log, in addiction of the filename
     *
     * Example of a log message : "2019-11-19 15:05:38 INFO - main.cc : Database connection success"
     *
     * \param std::string str : the string that will be writen in the log file
     */
    void log_info(std::string);

    /**
     * \brief Write the string passed in parameter as a debug log, in addiction of the filename
     *
     * \param std::string str : the string that will be writen in the log file
     */
    void log_debug(std::string);

    /**
     * \brief Write the string passed in parameter a warn log, in addiction of the filename
     *
     * \param std::string str : the string that will be writen in the log file
     */
    void log_warn(std::string);

    /**
     * \brief Write the string passed in parameter as an error log, in addiction of the filename
     *
     * \param std::string str : the string that will be writen in the log file
     */
    void log_error(std::string);

    /**
     * \brief Write the string passed in parameter as a trace log, in addiction of the filename
     *
     * \param std::string str : the string that will be writen in the log file
     */
    void log_trace(std::string);

    /**
     * \brief Write the string passed in parameter as a fatal log, in addiction of the filename
     *
     * \param std::string str : the string that will be writen in the log file
     */
    void log_fatal(std::string);
};
#endif //Q_PIPELIER_V_TAZIR_RAID_DOSSARD_LOGGER_H
