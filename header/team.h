#ifndef TEAM_H
#define TEAM_H
#include <string>
#include <postgresql/libpq-fe.h>
#include "raid.h"

/**
 * \class Team
 *
 * \brief Represent a team
 */
class Team {
private:
  int number;
  Raid *the_raid;
  std::string name;
  std::string record_date;
  int global_time;
  int ranking;
  int penalties;

public:
  /**
   * \brief Unique Constructor which initialize the attributes with the values of the parameters
   *
   * \param int number : Team number
   *
   * \param std::string code_raid : Code of the raid related in which the team will participate
   *
   * \param std::string schema : Schema of the database in witch to retrieve the data
   *
   * \param Pgconn * connexion : handler for the database connexion
   */
  Team(int, std::string, std::string, PGconn *);
  
  /**
  * \brief Destructor
  */
  ~Team();

  /**
   * \brief Get the number of the Team
   *
   * \return number : the number of the Team as an integer
   */
  int getNumber();
  
  /**
   * \brief Get the Raid object of the Team
   *
   * \return the_raid : the raid in which the Team will participate as a Raid *object
   */
  Raid *getTheRaid();
  
  /**
   * \brief Get the name of the Team
   *
   * \return name : the name of the Team as a std::string
   */
  std::string getName();
  
  /**
   * \brief Get the record_date of the Team
   *
   * \return record_date : the date when the Team recorded to participate as a std::string
   */
  std::string getRecordDate();
  
  /**
   * \brief Get the global time of the Team
   *
   * \return global_time : the global time the Team participated as an integer
   */
  int getGlobalTime();
  
  /**
   * \brief Get the ranking of the Team
   *
   * \return ranking : the ranking of the Team during the raid as an integer
   */
  int getRanking();
  
  /**
   * \brief Get the penalties of the Team
   *
   * \return penalties : the penalties of the Team as an integer
   */
  int getPenalties();

  /**
   * \brief Mutate the number attribute with the value of the parameter
   *
   * \param int number - the new number of the Team
   */
  void setNumber(int);
  
  /**
   * \brief Mutate the the_raid attribute with the value of the parameter
   *
   * \param Raid* the_raid - the new related raid of the Team
   */
  void setTheRaid(Raid *);
  
  /**
   * \brief Mutate the name attribute with the value of the parameter
   *
   * \param std::string name - the new name of the Team
   */
  void setName(std::string);
  
  /**
   * \brief Mutate the record_date attribute with the value of the parameter
   *
   * \param std::string record_date - the new record_date of the Team
   */
  void setRecordDate(std::string);
  
  /**
   * \brief Mutate the global_time attribute with the value of the parameter
   *
   * \param int global_time - the new global_time of the Team
   */
  void setGlobalTime(int);
  
  /**
   * \brief Mutate the ranking attribute with the value of the parameter
   *
   * \param int ranking - the new ranking of the Team
   */
  void setRanking(int);
  
  /**
   * \brief Mutate the penalties attribute with the value of the parameter
   *
   * \param int penalties - the new penalties of the Team
   */
  void setPenalties(int);

};
#endif //EQUIPE_H
