#ifndef RUNNER_H
#define RUNNER_H
#include <string>
#include <postgresql/libpq-fe.h>

class Runner{
private:
  std::string licence;
  std::string name;
  std::string surname;
  char gender;
  std::string nationality;
  std::string mail;
  bool certif_med_aptitude;
  std::string birthdate;

public:
  /**
   * \brief Unique Constructor which initialize the attributes with the values of the parameters
   *
   * \param std::string licence : Licence of the runner
   *
   * \param std::string schema : Schema of the database in witch to retrieve the data
   *
   * \param Pgconn* connexion : handler for the database connexion
   */
  Runner(std::string, std::string, PGconn *);
  
  /**
  * \brief Destructor
  */
  ~Runner();

  /**
   * \brief Get the licence of the Runner
   *
   * \return licence : the licence of the runner as a std::string
   */
  std::string getLicence();
  
  /**
   * \brief Get the name of the Runner
   *
   * \return name : the name of the runner as a std::string
   */
  std::string getName();
  
  /**
   * \brief Get the surname of the Runner
   *
   * \return surname : the surname of the runner as a std::string
   */
  std::string getSurname();
  
  /**
   * \brief Get the gender of the Runner
   *
   * \return gender : the gender of the runner as a character (M or F)
   */
  char getGender();
  
  /**
   * \brief Get the nationality of the Runner
   *
   * \return nationality : the nationality of the runner as a std::string
   */
  std::string getNationality();
  
  /**
   * \brief Get the mail of the Runner
   *
   * \return mail : the e-mail address of the runner as a std::string
   */
  std::string getMail();
  
  /**
   * \brief Get the certificate of aptitude of the Runner
   *
   * \return certif_med_aptitude : the certificate of medical aptitude of the runner as a bool
   */
  bool getCertifMedAptitude();
  
  /**
   * \brief Get the birthdate of the Runner
   *
   * \return birthdate : the birthdate of the runner as a std::string
   */
  std::string getBirthdate();

  /**
   * \brief Mutate the licence attribute with the value of the parameter
   *
   * \param std::string licence - the new licence of the Runner
   */
  void setLicence(std::string);
  
  /**
   * \brief Mutate the name attribute with the value of the parameter
   *
   * \param std::string name - the new name of the Runner
   */
  void setName(std::string);
  
  /**
   * \brief Mutate the surname attribute with the value of the parameter
   *
   * \param std::string surname - the new surname of the Runner
   */
  void setSurname(std::string);
  
  /**
   * \brief Mutate the gender attribute with the value of the parameter
   *
   * \param char gender - the new gender of the Runner (M or F)
   */
  void setGender(char);
  
  /**
   * \brief Mutate the nationality attribute with the value of the parameter
   *
   * \param std::string nationality - the new nationality of the Runner
   */
  void setNationality(std::string);
  
  /**
   * \brief Mutate the mail attribute with the value of the parameter
   *
   * \param std::string mail - the new e-mail address of the Runner
   */
  void setMail(std::string);
  
  /**
   * \brief Mutate the certificate of aptitude with the value of the parameter
   *
   * \param bool certif_med_aptitude - the new catificate of medical aptitude of the Runner
   */
  void setCertifMedAptitude(bool);
  
  /**
   * \brief Mutate the birthdate attribute with the value of the parameter
   *
   * \param std::string birthdate - the new birthdate of the Runner
   */
  void setBirthdate(std::string);  
};
#endif //RUNNER_H
