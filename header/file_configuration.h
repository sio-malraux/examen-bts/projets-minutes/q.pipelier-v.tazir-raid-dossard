//
// Created by quentin on 05/05/2020.
//

#ifndef Q_PIPELIER_V_TAZIR_RAID_DOSSARD_FILE_CONFIGURATION_H
#define Q_PIPELIER_V_TAZIR_RAID_DOSSARD_FILE_CONFIGURATION_H

#include <iostream>
#include <fstream>
#include "jsoncpp/json/json.h"

/**
 * \class FileConfiguration
 *
 * \brief A class made to ease the use of JSON configuration file
 */
class FileConfiguration{
public:
   /**
	* \brief Unique constructor which gives a default value to the attributes
	*/
    FileConfiguration();

   /**
	* \brief Open and read the file to give values to the attributes  
	*
	* \param std::string path_config : the path to the JSON config file that will be opened
	*/
    void load(std::string path_config);

  /**
   * \brief Get the Database host name from the config file
   *
   * \return config_db_host : the database host name as a std::string
   */
    std::string getDbHost();
	
   /**
    * \brief Get the Database name from the config file
    *
    * \return config_db_name : the database name as a std::string
    */
    std::string getDbName();
	
   /**
    * \brief Get the Database user name from the config file
    *
    * \return config_db_user : the database user name as a std::string
    */
    std::string getDbUser();
	
   /**
    * \brief Get the Database user password from the config file
    *
    * \return config_db_password : the user's password as a std::string
    */
    std::string getDbPassword();
	
   /**
    * \brief Get the Database schema name from the config file
    *
    * \return config_db_schema : the name of the schema as a std::string
    */
    std::string getDbSchema();
	
  /**
   * \brief Get the Database port from the config file
   *
   * \return config_db_port : the user's port as a std::string
   */
    std::string getDbPort();

   /**
    * \brief Verify if the config file is loaded
    *
    * \return bool config_loaded : true if the config file has been loaded, false otherwise
	*/
    bool getConfigLoaded();

private:
    std::string config_db_host;
    std::string config_db_name;
    std::string config_db_user;
    std::string config_db_password;
    std::string config_db_schema;
    std::string config_db_port;
    bool config_loaded;
};
#endif //Q_PIPELIER_V_TAZIR_RAID_DOSSARD_FILE_CONFIGURATION_H
