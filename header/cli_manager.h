//
// Created by quentin on 05/05/2020.
//

#ifndef Q_PIPELIER_V_TAZIR_RAID_DOSSARD_CLI_MANAGER_H
#define Q_PIPELIER_V_TAZIR_RAID_DOSSARD_CLI_MANAGER_H

#include <iostream>
#include <gflags/gflags.h>
#include <gflags/gflags_declare.h>

/**
 * \file manage_cli.h
 * \brief File used to manage the CLI
 *
 * Declare all the parameter of the command line
 */
DECLARE_string(db_host);
DECLARE_string(db_name);
DECLARE_string(db_schema);
DECLARE_string(db_user);
DECLARE_string(db_password);
DECLARE_string(db_port);
DECLARE_string(config_file);
DECLARE_string(raid_code);

void configureCommandLine();

#endif //Q_PIPELIER_V_TAZIR_RAID_DOSSARD_CLI_MANAGER_H
