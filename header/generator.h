// // Created by quentin on 05/05/2020. 

#ifndef Q_PIPELIER_V_TAZIR_RAID_DOSSARD_GENERATOR_H 
#define Q_PIPELIER_V_TAZIR_RAID_DOSSARD_GENERATOR_H 

#include <iostream>
#include <string>
#include <postgresql/libpq-fe.h>
#include <stdlib.h>
#include <vector>

/**
 * \class Generator
 * \brief Class used to generate a number for the runner_shirt
 */
class Generator{
private:
    bool generated = false;
    bool exist = false;
    PGconn *connection;
    std::string schema;
    std::string code_raid; 
    std::string result_message;
	
public:
  /**
   * \brief Unique Constructor which initialize the attributes with the values of the parameters
   *
   * \param std::string code_raid : Code of the raid in witch the runner will participate
   *
   * \param std::string schema : Database schema where the data is located
   *
   * \param Pgconn * connexion : handler for the database connexion
   */
    Generator(std::string, std::string, PGconn *);
	
  /**
   * \brief Destructor
   */
    ~Generator();
	
  /**
   * \brief Method to verify if the shirt_number has already been generated. If it isn't, call the method insertDossardString
   */
    std::string generateDossard();
	
  /**
   * \brief Generate the shirt_number of all the runners
   *
   * \return all_dossard_string : a list of all the dossard string generated for a raid as a std::vector<std::string> 
   */
  std::vector<std::string> generateDossardString(); 
};
#endif //Q_PIPELIER_V_TAZIR_RAID_DOSSARD_GENERATOR_H

