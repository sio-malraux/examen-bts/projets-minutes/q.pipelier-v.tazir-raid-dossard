SET client_encoding = 'UTF8';
DROP SCHEMA IF EXISTS "raid_dossard" CASCADE;;
CREATE SCHEMA "raid_dossard";

CREATE TABLE "raid_dossard"."activite" (
    "code" character varying(5) NOT NULL,
    "libelle" character varying(50)
);
CREATE TABLE "raid_dossard"."activite_raid" (
    "code_raid" character varying(5) NOT NULL,
    "code_activite" character varying(5) NOT NULL
);
CREATE TABLE "raid_dossard"."coureur" (
    "licence" character varying(5) NOT NULL,
    "nom" character varying(50),
    "prenom" character varying(50),
    "sexe" character(1),
    "nationalite" character varying(50),
    "mail" character varying(50),
    "certif_med_aptitude" boolean DEFAULT false,
    "date_naissance" "date"
);
CREATE TABLE "raid_dossard"."equipe" (
    "numero" integer NOT NULL,
    "code_raid" character varying(5) NOT NULL,
    "nom" character varying(50),
    "date_inscription" "date" DEFAULT "now"(),
    "temps_global" integer DEFAULT 0,
    "classement" integer DEFAULT 0,
    "penalites_bonif" integer DEFAULT 0
);
CREATE TABLE "raid_dossard"."integrer_equipe" (
    "numero_equipe" integer NOT NULL,
    "code_raid" character varying(5) NOT NULL,
    "licence" character varying(5) NOT NULL,
    "temps_individuel" integer DEFAULT 0,
    "num_dossard" character varying(15)
);
CREATE TABLE "raid_dossard"."raid" (
    "code" character varying(5) NOT NULL,
    "nom" character varying(50),
    "date_debut" character varying(10),
    "ville" character varying(50),
    "region" character varying(50),
    "nb_maxi_par_equipe" integer,
    "montant_inscription" numeric(8,2),
    "nb_femmes" integer,
    "duree_maxi" integer,
    "age_minimum" integer
);
ALTER TABLE ONLY "raid_dossard"."activite_raid"
    ADD CONSTRAINT "activite_raid_pkey" PRIMARY KEY ("code_raid", "code_activite");
ALTER TABLE ONLY "raid_dossard"."activite"
    ADD CONSTRAINT "pk_activite" PRIMARY KEY ("code");
ALTER TABLE ONLY "raid_dossard"."coureur"
    ADD CONSTRAINT "pk_coureur" PRIMARY KEY ("licence");
ALTER TABLE ONLY "raid_dossard"."equipe"
    ADD CONSTRAINT "pk_equipe" PRIMARY KEY ("numero", "code_raid");
ALTER TABLE ONLY "raid_dossard"."integrer_equipe"
    ADD CONSTRAINT "pk_integrer" PRIMARY KEY ("numero_equipe", "code_raid", "licence");
ALTER TABLE ONLY "raid_dossard"."raid"
    ADD CONSTRAINT "pk_raid" PRIMARY KEY ("code");

ALTER TABLE ONLY "raid_dossard"."activite_raid"
    ADD CONSTRAINT "activite_raid_code_activite_fkey" FOREIGN KEY ("code_activite") REFERENCES "raid_dossard"."activite"("code");
ALTER TABLE ONLY "raid_dossard"."activite_raid"
    ADD CONSTRAINT "activite_raid_code_raid_fkey" FOREIGN KEY ("code_raid") REFERENCES "raid_dossard"."raid"("code");
ALTER TABLE ONLY "raid_dossard"."equipe"
    ADD CONSTRAINT "equipe_code_raid_fkey" FOREIGN KEY ("code_raid") REFERENCES "raid_dossard"."raid"("code");
ALTER TABLE ONLY "raid_dossard"."integrer_equipe"
    ADD CONSTRAINT "inscrire_individuel_code_coureur_fkey" FOREIGN KEY ("licence") REFERENCES "raid_dossard"."coureur"("licence");
ALTER TABLE ONLY "raid_dossard"."integrer_equipe"
    ADD CONSTRAINT "inscrire_individuel_equipe_fkey" FOREIGN KEY ("numero_equipe", "code_raid") REFERENCES "raid_dossard"."equipe"("numero", "code_raid");
GRANT USAGE ON SCHEMA "raid_dossard" TO "etudiants-slam";
GRANT SELECT ON TABLE "raid_dossard"."activite" TO "etudiants-slam";
GRANT SELECT ON TABLE "raid_dossard"."activite_raid" TO "etudiants-slam";
GRANT SELECT ON TABLE "raid_dossard"."coureur" TO "etudiants-slam";
GRANT SELECT ON TABLE "raid_dossard"."equipe" TO "etudiants-slam";
GRANT SELECT ON TABLE "raid_dossard"."integrer_equipe" TO "etudiants-slam";
GRANT SELECT ON TABLE "raid_dossard"."raid" TO "etudiants-slam";
