SET SCHEMA 'raid_dossard';

INSERT INTO activite (code, libelle) VALUES
	('VTT01', 'VTT'),
	('TRK02','trekking'),
	('CHV03','cheval'),
	('ORI04','orientation'),
	('CAN05','canyoning'),
	('MON06','montagne'),
	('EAU07','eaux vives'),
	('COR08','cordes');
	
-- Duree en jours
INSERT INTO raid (code, nom, date_debut, ville, region, nb_maxi_par_equipe, montant_inscription, nb_femmes, duree_maxi, age_minimum) VALUES
	('RAID1', 'Le raid de la Loire', '10-10-2020', 'Nantes', 'Pays-de-la-Loire', 6, 140.22, 0, 7, 17),
	('RAID2','Raid numero 2', '04-03-2021', 'Une ville', 'Une région', 9, 34.28, 3, 6, 20);
	
INSERT INTO activite_raid (code_raid, code_activite) VALUES
	('RAID1', 'VTT01'),
	('RAID1', 'CHV03'),
	('RAID1', 'ORI04'),
	('RAID1', 'EAU07'),
	('RAID1', 'COR08'),
	('RAID2','VTT01'),
	('RAID2','TRK02'),
	('RAID2','CAN05'),
	('RAID2','COR08'),
	('RAID2','MON06');

-- temps en jours
INSERT INTO equipe (numero, code_raid, nom, date_inscription, temps_global, classement, penalites_bonif) VALUES
	(1, 'RAID1', 'les winners', '02-02-2020', 6, 2, 2),
	(2, 'RAID1', 'un nom', '16-03-2020', 5, 1, 0),
	(3, 'RAID1', 'aucune idee', '27-01-2020', 7, 3, 1),
	(4, 'RAID2', 'equipe 1', '07-12-2019', 9, 2, 4),
	(5, 'RAID2', 'equipe 2', '15-04-2020', 6, 1, 0);
	
INSERT INTO coureur (licence, nom, prenom, sexe, nationalite, mail, certif_med_aptitude, date_naissance) VALUES
	('CA-46', 'Attends', 'Charles', 'M', 'Francais','c.a@yahoo.com', false, '10-10-2010'),
	('FP-66', 'Pierre', 'Feuille', 'M', 'Francais', 'p.f@caramail.az', true, '01-01-1997'),
	('CF-34', 'Feuille', 'Ciseaux', 'M', 'Francais', 'c.f@hotmail.az', true, '06-04-2001'),
	('FP-86', 'Pierre', 'Feuille', 'M', 'Francais', 'p.f@gmailorg', true, '01-01-1997'),
	('AT-21', 'Terrieur', 'Alex', 'M', 'Francais', 'a.t@gmail.com', true, '15-08-1999'),
	('AT-23', 'Terrieur', 'Alain', 'M', 'Francais', 'a.t@framagit.org', false, '04-05-1995'),
	('BJ-36', 'Jean', 'Bernard', 'M', 'Francais', 'b.j@tacos.es', true, '07-08-1992'),
	('FF-P2', 'Nom', 'Prenom', 'M', 'Francais', 'nom.prenom@machin.truc', true, '01-01-1991'),
	('03615', 'Georges', 'Celine', 'F', 'Francais', 'c.g@help.me', true, '25-07-2003'),
	('M1911', 'Lafeuille', 'Albert', 'M', 'Francais', 'test@sio.gg', true, '13-11-1989'),
	('AK-47', 'Bon', 'Jean', 'M', 'Francais', 'jean.bon@cosme.fr', true, '01-01-1997'),
	('0M-16', 'Rienfait', 'Gerard', 'M', 'Francais', 'g.rienfait@prison.org', false, '05-07-1999'),
	('36-30', 'Wich', 'Sandy', 'F', 'Francais', 'sand.wich@mail.mel', true, '01-01-1994'),
	('10-22', 'Machin', 'Truc', 'M', 'Francais', 'a.b@mail.per', true, '01-01-1990'),
	('16-38', 'Pludidee', 'Georges', 'M', 'Francais', 'g.pludidee@aidez.moa', true, '01-01-1986');

-- temps en jours
INSERT INTO integrer_equipe (numero_equipe, code_raid, licence, temps_individuel, num_dossard) VALUES
	(1, 'RAID1', 'CA-46', 2, '1-RAID1-AC-1'),
	(1, 'RAID1', 'FP-66', 3, '1-RAID1-PF-2'),
	(1, 'RAID1', 'CF-34', 4, '1-RAID1-FC-3'),
	(2, 'RAID1', 'FP-86', 5, '2-RAID1-FP-1'),
	(2, 'RAID1', 'AT-21', 6, '2-RAID1-TA-2'),
	(2, 'RAID1', 'AT-23', 3, '2-RAID1-TA-3'),
	(3, 'RAID1', 'BJ-36', 4, '3-RAID1-JB-1'),
	(3, 'RAID1', 'FF-P2', 7, '3-RAID1-NP-2'),
	(3, 'RAID1', '03615', 5, '3-RAID1-GC-3'),
	(4, 'RAID2', 'M1911', 4, '4-RAID2-LA-1'),
	(4, 'RAID2', 'AK-47', 2, '4-RAID2-BJ-2'),
	(4, 'RAID2', '0M-16', 6, '4-RAID2-RG-3'),
	(5, 'RAID2', '36-30', 4, '5-RAID2-WS-1'),
	(5, 'RAID2', '10-22', 2, '5-RAID2-MT-2'),
	(5, 'RAID2', '16-38', 6, '5-RAID2-PG-3');